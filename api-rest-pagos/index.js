'use strict'

const port = process.env.PORT || 3333; 

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};



const express = require('express'); //añadir libreria
const app = express();
const logger = require('morgan');
const mongojs = require('mongojs');

var dbc = mongojs('mongodb+srv://marc:barcelona2000@cluster0.pnvdh.mongodb.net/coches?retryWrites=true&w=majority');     //Conectamos con la bd
var dbv = mongojs('mongodb+srv://marc:barcelona2000@cluster0.pnvdh.mongodb.net/vuelos?retryWrites=true&w=majority');     //Conectamos con la bd
var dbh = mongojs('mongodb+srv://marc:barcelona2000@cluster0.pnvdh.mongodb.net/hoteles?retryWrites=true&w=majority');     //Conectamos con la bd

var id = mongojs.ObjectID;

//Declaramos los Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended:false}));  //con esto se reconoce el código puesto en postman
app.use(express.json()); //para que postman sepa tratar con objetos json

//el parámetro colecciones es lo que se pasaba
//como db.collection(queColeccion)
app.param('colecciones', (req, res, next, colecciones) => {
    console.log('middleware param /api/:colecciones');
    req.collection = db.collection(colecciones);
    return next();
});

// Declaramos nuestro middleware de autorización
function auth (req, res, next){
    if(!req.headers.authorization) {
        res.status(401).json ({
            result: 'KO',
            mensaje: "No se ha enviado el token en la cabecera token."
        });
        return next(new Error("Falta token de autorización"));
    }

    console.log(req.headers.authorization);

    if(req.headers.authorization.split(" ")[1] === "MITOKEN123456789"){ //JWT
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });
    return next(new Error("Acceso no autorizado."));
}

app.get('/pagos/coches', (req,res,next) => {
    let p = Math.random(0,10);
    const queId = req.body._id;
    const nuevosDatos = {
        estado: 'pagado'
    }

    if(p < 9){
        
        dbc.coches.findOne({_id: id(queId)}, {estado: 'reservado'}, (err, elemento) => {
            if (err) return next(err);
                dbc.coches.update(
                    {_id: id(queId)},
                    {$set: nuevosDatos},
                    (err, elemento) => {
                        res.json({
                            result: 'OK, pago realizado correctamente'
                        });
                    }
                )
        });
    }
    else{
        res.json({
            result: 'ERROR, pago no aceptado'
        })
    }
});

app.get('/pagos/hoteles', (req,res,next) => {
    let p = Math.random(0,10);
    const queId = req.body._id;
    const nuevosDatos = {
        estado: 'pagado'
    }

    if(p < 9){
        
        dbh.hoteles.findOne({_id: id(queId)}, {estado: 'reservado'}, (err, elemento) => {
            if (err) return next(err);
                dbh.hoteles.update(
                    {_id: id(queId)},
                    {$set: nuevosDatos},
                    (err, elemento) => {
                        res.json({
                            result: 'OK, pago realizado correctamente'
                        });
                    }
                )
        });
    }
    else{
        res.json({
            result: 'ERROR, pago no aceptado'
        })
    }
});



app.get('/pagos/vuelos', (req,res,next) => {
    let p = Math.random(0,10);
    const queId = req.body._id;
    const nuevosDatos = {
        estado: 'pagado'
    }

    if(p < 9){
        
        dbv.vuelos.findOne({_id: id(queId)}, {estado: 'reservado'}, (err, elemento) => {
            if (err) return next(err);
                dbv.vuelos.update(
                    {_id: id(queId)},
                    {$set: nuevosDatos},
                    (err, elemento) => {
                        res.json({
                            result: 'OK, pago realizado correctamente'
                        });
                    }
                )
        });
    }
    else{
        res.json({
            result: 'ERROR, pago no aceptado'
        })
    }
});




https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`API PAGOS ejecutándose en https://localhost:${port}/api/:colecciones/:id`); //comillas invertidas para que me recoja el puerto
});



