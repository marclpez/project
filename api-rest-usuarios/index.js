'use strict'

const port = process.env.PORT || 3555; 

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};


const moment = require('moment');
const jwt = require('jwt-simple');
const express = require('express'); //añadir libreria
const app = express();
const logger = require('morgan');
const mongojs = require('mongojs');
const bcrypt = require('bcrypt');


var db = mongojs('mongodb+srv://marc:barcelona2000@cluster0.pnvdh.mongodb.net/usuarios?retryWrites=true&w=majority');     //Conectamos con la bd
var id = mongojs.ObjectID;

//Declaramos los Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended:false}));  //con esto se reconoce el código puesto en postman
app.use(express.json()); //para que postman sepa tratar con objetos json

//el parámetro colecciones es lo que se pasaba
//como db.collection(queColeccion)
app.param('colecciones', (req, res, next, colecciones) => {
    console.log('middleware param /api/:colecciones');
    req.collection = db.collection(colecciones);
    return next();
});


// Declaramos nuestro middleware de autorización
function auth (req, res, next){
    if(!req.headers.authorization) {
        res.status(401).json ({
            result: 'KO',
            mensaje: "No se ha enviado el token en la cabecera token."
        });
        return next(new Error("Falta token de autorización"));
    }

    console.log(req.headers.authorization);

    if(!db.usuarios.findOne({token: jwt.encode(req.body.email, "MITOKEN123456789")})){
        res.status(401).json({
            result: 'KO',
            mensaje: "Necesitas loggearte"
        });
        return next(new Error("Necesitas iniciar sesión"));
    }

    if(db.usuarios.findOne({token: jwt.encode(req.body.email, "MITOKEN123456789")})){
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });


    return next(new Error("Acceso no autorizado."));
}


//Declaramos nuestras rutas y nuestros controladores
app.get('/api', (req, res, next) => {
    db.getCollectionNames((err, colecciones) =>{
        if (err) return next(err);   //Propagamos el error

        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });
    });
});

app.get('/api/:colecciones', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;

    req.collection.find((err, elementos) => {
        if (err) return next(err);

        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: queColeccion,
            elementos: elementos
        });
    });
});

//Get de un elemento
app.get('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queId = req.params.id;

    req.collection.findOne({ _id: id(queId) },(err, elementos) => {
        if (err) return next(err);

        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: queColeccion,
            elemento: elemento
        });
    });
});

//Modificar un usuario
app.put('/api/:colecciones/:email', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const nuevosDatos = req.body;
    const queEmail = req.params.email;

    req.collection.update(
        { email: queEmail},
        { $set: nuevosDatos },
        { safe: true, multi: false},
        (err, resultado) => {
            if (err) return next(err);

            res.json({
                result: 'OK',
                colección: queColeccion,
                resultado: nuevosDatos
            });
        }
    );
});

//Eliminar un usuario
app.delete('/api/:colecciones/:email', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queId = req.params.id;
    
    //Con el middleware apunta a la tabla que nos interesa
    req.collection.remove(
        { _id: id(queId) },
        (err, resultado) => {
            if (err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                colección: queColeccion,
                eleemnto: queId,
                resultado: resultado
            });
        }
    );

});

//Crear un usuario
app.post('/api/:colecciones/registrar', (req, res, next) => {
    const nuevoUsuario = {
        email: req.body.email,
        nombre: req.body.nombre,
        password: bcrypt.hashSync(req.body.password, 10),
        token: ""
    };
    const queColeccion = req.params.colecciones;
    

    req.collection.findOne( {email: req.body.email}, (err, usuario) => {
        if (usuario) {
            return res.status(400).send('Este correo ya está registrado');
        }
        else{

            req.collection.save(nuevoUsuario, (err, elementoGuardado) => {
                if(err){
                    next(err); //propagamos el error
                }
                console.log(elementoGuardado);  //lo que ha guardado
                    res.status(201).json ({
                        result: 'OK',
                        colección: queColeccion,
                        elemento: elementoGuardado
                    });
            });
        }
    });
});


app.put('/login', (req,res,next) => {
    const nuevoUsuario = {
        email: req.body.email,
        nombre: req.body.nombre,
        password: req.body.nombre
    };   

    db.usuarios.findOne( {email: req.body.email}, (err, usuarioobt) => {
        if (!usuarioobt || !bcrypt.compareSync(req.body.password, usuarioobt.password)) {
            return res.status(400).send('Error de usuario');
        }
        else{
            const payload = {
                sub: usuarioobt._id,
                iat: moment().unix(),
                exp: moment().add(1, 'days').unix()
            }
            
            const token = jwt.encode(payload, 'MITOKEN123456789');
            db.usuarios.update({email: req.body.email}, {$set : {"token":token}})
            res.status(201).json ({
                result: 'OK',
                //email: usuarioobt,
                token: token
            });
        }
    });
});


https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`SEC WS API REST CRUD con BD ejecutándose en https://localhost:${port}/api/:colecciones/:id`); //comillas invertidas para que me recoja el puerto
});

