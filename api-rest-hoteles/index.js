'use strict'

const port = process.env.PORT || 3300; 

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};


const express = require('express'); //añadir libreria
const app = express();
const logger = require('morgan');
const mongojs = require('mongojs');

var db = mongojs('mongodb+srv://marc:barcelona2000@cluster0.pnvdh.mongodb.net/hoteles?retryWrites=true&w=majority');     //Conectamos con la bd
var id = mongojs.ObjectID;

//Declaramos los Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended:false}));  //con esto se reconoce el código puesto en postman
app.use(express.json()); //para que postman sepa tratar con objetos json

//el parámetro colecciones es lo que se pasaba
//como db.collection(queColeccion)
app.param('colecciones', (req, res, next, colecciones) => {
    console.log('middleware param /api/:colecciones');
    req.collection = db.collection(colecciones);
    return next();
});

// Declaramos nuestro middleware de autorización
function auth (req, res, next){
    if(!req.headers.authorization) {
        res.status(401).json ({
            result: 'KO',
            mensaje: "No se ha enviado el token en la cabecera token."
        });
        return next(new Error("Falta token de autorización"));
    }

    if(req.headers.authorization.split(" ")[1] === "MITOKEN123456789"){ //JWT
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });

    return next(new Error("Acceso no autorizado."));
}


//Declaramos nuestras rutas y nuestros controladores
app.get('/api', (req, res, next) => {
    db.getCollectionNames((err, colecciones) =>{
        if (err) return next(err);   //Propagamos el error

        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });
    });
});

app.get('/api/:colecciones', (req, res, next) => {
    const queColeccion = req.params.colecciones;

    req.collection.find((err, elementos) => {
        if (err) return next(err);

        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: queColeccion,
            elementos: elementos
        });
    });
});

//Get de un elemento
app.get('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queId = req.params.id;

    req.collection.findOne({ _id: id(queId), estado: "libre"},(err, elemento) => {
        if (err) return next(err);

        if(elemento == null){
            res.json({
                result: 'El hotel ya está reservado'
            });
        }
        else{
            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: queColeccion,
                elemento: elemento
            });
        }
    });
});

//No se hace post de tablas porque si la tabla no existe se crea sola
//Guardar los datos de un cliente desde una máquina distinta
app.post('/api/:colecciones', (req, res, next) => {
    const nuevoElemento = req.body; //lo que yo quiero guardar
    const queColeccion = req.params.colecciones;

    //se crea aqui la tabla si no existiese
    req.collection.save(nuevoElemento, (err, elementoGuardado) =>{
        if (err) return next(err);

        console.log(elementoGuardado);  //lo que ha guardado
        res.status(201).json ({
            result: 'OK',
            colección: queColeccion,
            elemento: elementoGuardado
        });
    });
});

//Modificar un producto
app.put('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const nuevosDatos = req.body;
    const queId = req.params.id;

    req.collection.update(
        { _id: id(queId) },
        { $set: nuevosDatos },
        { safe: true, multi: false},
        (err, resultado) => {
            if (err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                colección: queColeccion,
                resultado: resultado
            });
        }
    );

});


app.put('/api/:colecciones/reservar/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const nuevosDatos = {
        estado: 'reservado'
    }
    const queId = req.params.id;

    req.collection.update(
        { _id: id(queId) },
        { $set:  nuevosDatos},
        { safe: true, multi: false},
        (err, resultado) => {
            if (err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                colección: queColeccion,
                resultado: resultado
            });
        }
    );

});

//Modificar un producto
app.delete('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queId = req.params.id;
    
    //Con el middleware apunta a la tabla que nos interesa
    req.collection.remove(
        { _id: id(queId) },
        (err, resultado) => {
            if (err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                colección: queColeccion,
                eleemnto: queId,
                resultado: resultado
            });
        }
    );

});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`SEC WS API REST CRUD con BD ejecutándose en https://localhost:${port}/api/:colecciones/:id`); //comillas invertidas para que me recoja el puerto
});
