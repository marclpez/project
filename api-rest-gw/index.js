'use strict'

const port = process.env.PORT || 4100; 
const URL_WS = 'https://localhost:?/api';

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};




const express = require('express'); //añadir libreria
const app = express();
const logger = require('morgan');
const fetch = require('node-fetch');    //se declara un cliente



//Declaramos los Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended:false}));  //con esto se reconoce el código puesto en postman
app.use(express.json()); //para que postman sepa tratar con objetos json


// Declaramos nuestro middleware de autorización
function auth (req, res, next){
    if(!req.headers.authorization) {
        res.status(401).json ({
            result: 'KO',
            mensaje: "No se ha enviado el token en la cabecera token."
        });
        return next(new Error("Falta token de autorización"));
    }

    console.log(req.headers.authorization);

    const queToken = req.headers.authorization.split(" ")[1];

    if(queToken === "MITOKEN123456789" ){ //JWT

        req.params.token = queToken;        //Creamos nueva propiedad para propagar el token
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });
    return next(new Error("Acceso no autorizado."));
}

/** 
//Declaramos nuestras rutas y nuestros controladores
app.get('/api', (req, res, next) => {
    /** db.getCollectionNames((err, colecciones) =>{
        if (err) return next(err);   //Propagamos el error

        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });
    }); 
});*/

app.get('/api/:colecciones', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queURL = `${URL_WS}/${queColeccion}`;

    fetch( queURL )
        .then( resp=> resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json( {
                resultado: json.result,
                colecciones: queColeccion,
                elementos: json.elementos
            });
    });
});

//Get de un elemento
app.get('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queId = req.params.id;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;

    fetch( queURL )
        .then( resp=> resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json( {
                resultado: json.result,
                colecciones: queColeccion,
                elemento: json.elementos
            });
    });
});

//No se hace post de tablas porque si la tabla no existe se crea sola
//Guardar los datos de un cliente desde una máquina distinta
app.post('/api/:colecciones', auth, (req, res, next) => {
    const nuevoElemento = req.body; //lo que yo quiero guardar
    const queColeccion = req.params.colecciones;
    const queURL = `${URL_WS}/${queColeccion}`;
    const queToken = req.params.token;

    fetch( queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoElemento),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`
                        }
                   } )
        .then( resp=> resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json( {
                result: 'OK',
                colecciones: queColeccion,
                elemento: json.elemento
            });
    });

});

//Modificar un producto
app.put('/api/:colecciones/:id', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const nuevosDatos = req.body;
    const queId = req.params.id;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;
    const queToken = req.params.token;

    fetch( queURL, {
                        method: 'PUT',
                        body: JSON.stringify(nuevosDatos),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`
                        }
                   } )
        .then( resp=> resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json( {
                result: 'OK',
                colecciones: queColeccion,
                elementos: json.elemento
            });
    });

});

//Eliminar un producto
app.delete('/api/:colecciones/:id', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queId = req.params.id;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;
    const queToken = req.params.token;
    
    fetch( queURL, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`
                        }
                   } )
        .then( resp=> resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json( {
                result: 'OK',
                colecciones: queColeccion
            });
    });

});


https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API GW del WS REST CRUD con BD ejecutándose en http://localhost:${port}/api/:colecciones/:id`); //comillas invertidas para que me recoja el puerto
});
                                                   
